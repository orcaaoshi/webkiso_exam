<?php
$year = date('Y');
$month = date('n');
$header_row = "";
$main_row = "";
$start = 'sun';

if (isset($_POST['display'])) {
    $start = isset($_POST['start']) ? $_POST['start'] : 'sun';
    if (is_numeric($_POST['year']) && is_numeric($_POST['month'])) {
        $year  = $_POST['year'];
        $month = $_POST['month'];

        $today_year  = date('Y');
        $today_month = date('n');
        $today_day   = date('j');
        $is_this_year_month = ($year == $today_year && $month == $today_month);

        $timestamp = mktime(0, 0, 0, $month, 1, $year);
        // この月は何日まであるか？
        $last_day_of_month = date('t', $timestamp);
        if ($start == 'sun') {
            // 1日は何曜日か？
            $week_of_first_day = date('w', $timestamp);
            // 最終日は何曜日？
            $week_of_last_day = date('w', mktime(0,0,0,$month,$last_day_of_month,$year));
        } else {
            // 1日は何曜日か？
            $week_of_first_day = date('N', $timestamp) - 1;
            // 最終日は何曜日？
            $week_of_last_day = date('N', mktime(0,0,0,$month,$last_day_of_month,$year)) -1;
        }

        /*
        echo '1日は'.$week_of_first_day.'曜日';
        echo 'この月は'.$last_day_of_month.'日 まで';
        */
        if ($start == 'sun') {
        $header_row = <<<EOT
            <tr>
                <th>日</th>
                <th>月</th>
                <th>火</th>
                <th>水</th>
                <th>木</th>
                <th>金</th>
                <th>土</th>
            </tr>
EOT;
        } else {
        $header_row = <<<EOT
            <tr>
                <th>月</th>
                <th>火</th>
                <th>水</th>
                <th>木</th>
                <th>金</th>
                <th>土</th>
                <th>日</th>
            </tr>
EOT;
        }
        $main_row = '<tr>';
        // 最初の行？
        for ($i=0;$i<$week_of_first_day;$i++) {
            $main_row .= '<td></td>';
        }
        // 月内の日付を全て表示
        for ($day=1;$day<=$last_day_of_month;$day++) {
            if ($is_this_year_month && $today_day == $day) {
                $main_row .= '<td class="today">'.$day.'</td>';
            } else {
                $main_row .= '<td>'.$day.'</td>';
            }
            
            // 改行位置？
            if (($week_of_first_day+$day) % 7 == 0) {
                $main_row .= '</tr><tr>';
            }
        }
        // 最終行の残りを埋める
        for ($i=$week_of_last_day;$i<6;$i++) {
            $main_row .= '<td></td>';
        }
        // 行をしめくくる
        $main_row .= '</tr>';
    }

}
?>
<style>
input.right {
    text-align: right;
}
input.l4 {
    width: 4em;
}
input.l2 {
    width: 2em;
}
th, td {
    text-align: center;
}
td.today {
    background-color: #55DDFF;
    font-weight: bold;
}
</style>
<form method="post" action="" enctype="multipart/form-data">
    <input class="right l4" maxlength="4" type="text" name="year" value="<?=$year?>">年
    <input class="right l2" maxlength="2" type="text" name="month" value="<?=$month?>">月
    <br>
    <label><input type="radio" name="start" value="sun" <?= ($start=='sun')?'checked':''; ?>>日曜始まり</label>
    <label><input type="radio" name="start" value="mon" <?= ($start=='mon')?'checked':''; ?>>月曜始まり</label>
    <br>
    <input type="submit" name="display" value="表示">
</form>
<hr>
<table>
    <thead>
<?= $header_row; ?>
    </thead>
    <tbody>
<?= $main_row; ?>
    </tbody>
</table>