<?php
session_start();

$message = '';

if (isset($_POST['clear']) || ! isset($_SESSION['answer'])) {
    $_SESSION['answer'] = random_4digit();
    $_SESSION['try_count'] = 0;
    $_SESSION['history'] = '';
}

if (isset($_POST['try'])) {
    $try = $_POST['v'];
    if (isset($try[0]) 
     && isset($try[1])
     && isset($try[2])
     && isset($try[3])) {
        $_SESSION['try_count']++;
        $message = try_hit($try, $_SESSION['answer']);
        $_SESSION['history'] = 
                $_SESSION['try_count']. '回目: '.
                $try[0]. $try[1]. $try[2]. $try[3]. 
                ' ... '.
                $message. ' ！<br>' . $_SESSION['history'];
    } else {
        $message = '再度入力してください';
    }
}


function try_hit($try, $answer) {
    // 比較する
    $hits = array();
    $blows = array();

    // チェック
    if ($try[0] == $answer[0]) {
        // HIT!
        $hits[0] = true;
    } else {
        // blow?
        if ($try[0] == $answer[1]
         || $try[0] == $answer[2]
         || $try[0] == $answer[3]) {
            // blow!
            $blows[0] = true;
        }
    }
    if ($try[1] == $answer[1]) {
        $hits[1] = true;
    } else {
        // blow?
        if ($try[1] == $answer[0]
         || $try[1] == $answer[2]
         || $try[1] == $answer[3]) {
            // blow!
            $blows[1] = true;
        }
    }
    if ($try[2] == $answer[2]) {
        $hits[2] = true;
    } else {
        // blow?
        if ($try[2] == $answer[0]
         || $try[2] == $answer[1]
         || $try[2] == $answer[3]) {
            // blow!
            $blows[2] = true;
        }
    }
    if ($try[3] == $answer[3]) {
        $hits[3] = true;
    } else {
        // blow?
        if ($try[3] == $answer[0]
         || $try[3] == $answer[1]
         || $try[3] == $answer[2]) {
            // blow!
            $blows[3] = true;
        }
    }

    
    $hit = count($hits);
    $blow = count($blows);
    return "{$hit}H{$blow}B";
}

function random_4digit() {
    $digit = array();
    $digit[0] = random_number();
    while(true){
        $temp = random_number();
        if ($digit[0] != $temp) {
            $digit[1] = $temp;
            break;
        }
    }
    while(true){
        $temp = random_number();
        if ($digit[0] != $temp && $digit[1] != $temp) {
            $digit[2] = $temp;
            break;
        }
    }
    while(true){
        $temp = random_number();
        if ($digit[0] != $temp && $digit[1] != $temp && $digit[2] != $temp) {
            $digit[3] = $temp;
            break;
        }
    }
    return $digit;
}

function random_number() {
    return mt_rand(0,9);
}

?>
<style>
input.l1 {
    width: 1.2em;
}
</style>
<?php if ($message == '4H0B') : ?>
<strong>おめでとう！</strong><br>
<?= $_SESSION['try_count']; ?>回で正解。
<form method="post" action="" enctype="multipart/form-data">
    <input type="submit" name="clear" value="新しくはじめる">
</form>
<?php else: ?>
<form method="post" action="" enctype="multipart/form-data">
    <input class="l1" maxlength="1" type="text" name="v[]" value="">
    <input class="l1" maxlength="1" type="text" name="v[]" value="">
    <input class="l1" maxlength="1" type="text" name="v[]" value="">
    <input class="l1" maxlength="1" type="text" name="v[]" value="">
    <input type="submit" name="try" value="TRY">
</form>
<input type="hidden" name="debug" value="<?php var_dump($_SESSION['answer']); ?>">
<?php endif; ?>
<hr>
トライ数：<?= $_SESSION['try_count']; ?>
<div class="history">
<?php
// 回答状況表示
echo $_SESSION['history'];
?>
</div>